import Chalk from 'chalk';
import * as Path from 'path';
import FS from 'fs-extra';

import {
  config,
  download,
  exec,
  getBasenameFromURL,
  TEMP_DIR,
  OUT_DIR,
} from './@utils';

export interface DownloadMetaInfo {
  originalVideoPath: string;
  subPath: string;
  outputBasename: string;
  outputPath: string;
}

export async function downloadResources(): Promise<DownloadMetaInfo> {
  console.log(Chalk.green('Downloading resources...'));

  let downloadMeta = getDownloadMetaInfoFromConfig();

  console.log(Chalk.gray('Downloading video...'));
  await download(config.videoURL, downloadMeta.originalVideoPath);
  console.log(' - Finished downloading video.');

  console.log(Chalk.gray('Downloading subtitles...'));
  await download(config.subURL, downloadMeta.subPath);
  console.log(' - Finished downloading subtitles.');

  return downloadMeta;
}

function getDownloadMetaInfoFromConfig(): DownloadMetaInfo {
  let videoBasename = getBasenameFromURL(config.videoURL) || 'video.mkv';
  let originalVideoPath = Path.join(TEMP_DIR, videoBasename);

  let subBasename = getBasenameFromURL(config.subURL) || 'sub.ass';
  let subPath = Path.join(TEMP_DIR, subBasename);

  let name = Path.parse(videoBasename).name;
  let outputBasename = config.output || `${name}.mp4`;
  let outputPath = Path.join(OUT_DIR, outputBasename);

  return {
    originalVideoPath,
    subPath,
    outputBasename,
    outputPath,
  };
}
