import FS from 'fs-extra';
import Chalk from 'chalk';

export async function modifySubtitles(filepath: string): Promise<void> {
  console.log(Chalk.green('Modifying subtitles file...'));

  let subs = await FS.readFile(filepath, 'utf8');

  subs = correctFonts(subs);

  await FS.writeFile(filepath, subs);

  console.log(' - Finished.');
}

export function correctFonts(subs: string): string {
  // Replace Chinese with English names for systems not supporting Chinese characters
  subs = subs.replace(/文泉驿微米黑/g, 'WenQuanYi Micro Hei');

  return subs;
}
