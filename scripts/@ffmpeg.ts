import Chalk from 'chalk';
import {ordString} from 'fp-ts/lib/Ord';
import {exec} from './@utils';
import * as fs from 'fs/promises';

export async function burnSubtitles(
  videoPath: string,
  subPath: string,
  outPath: string,
  AD_CUT: string | undefined,
  AD_START: string | undefined,
  AD_END: string | undefined,
): Promise<void> {
  console.log(Chalk.green('Burning subtitles...'));

  if (AD_CUT && (AD_CUT == '1' || AD_CUT?.toLocaleLowerCase() == 'true')) {
    console.log("AD_CUT detected. Using Ad cut pipeline.")

    await exec(`mkfifo part1 part2`);
    await exec(
      `ffmpeg -y -i "${videoPath}" -force_key_frames:v ${AD_START},${AD_END} -c:v hevc_nvmpi -b:v 10M -c:a copy -vf "ass=${subPath}" -f mpegts burned.ts`,
    );

    await exec(
      `ffmpeg -y -i burned.ts -t ${AD_START} -c:v copy -c:a copy -f mpegts part1 2> /dev/null & \
        ffmpeg -y -i burned.ts -ss ${AD_END} -c:v copy -c:a copy -f mpegts part2 2> /dev/null & \
        ffmpeg -y -f mpegts -i "concat:part1|part2" -c:v copy -bsf:a aac_adtstoasc ${outPath}`,
    );

    await fs.unlink('burned.ts');
    await fs.unlink('part1');
    await fs.unlink('part2');
  } else {
    console.log("AD_CUT set to false. Using default pipeline.")
    await exec(
      `ffmpeg -i "${videoPath}" -c:v hevc_nvmpi -rc cbr -b:v 10M -vf "ass=${subPath}" "${outPath}"`,
    );
  }

  console.log(' - Finished.');
}
