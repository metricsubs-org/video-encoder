import * as Path from 'path';
import FS from 'fs-extra';
import Chalk from 'chalk';

import {config, Config, OUT_DIR, prepareFolders} from './@utils';
import {DownloadMetaInfo, downloadResources} from './@download';
import {modifySubtitles} from './@subs';
import {burnSubtitles} from './@ffmpeg';

async function main() {
  await prepareFolders();

  let downloadMeta = await downloadResources();

  await modifySubtitles(downloadMeta.subPath);

  let {originalVideoPath, subPath, outputPath} = downloadMeta;

  await burnSubtitles(originalVideoPath, subPath, outputPath, config.AD_CUT, config.AD_START, config.AD_END);

  await writeMeta(config, downloadMeta);
}

main().catch(console.error);

async function writeMeta(config: Config, downloadMeta: DownloadMetaInfo) {
  console.log(Chalk.green('Writing meta file...'));

  let metaPath = Path.join(OUT_DIR, 'meta.json');
  await FS.writeJSON(metaPath, {config, downloadMeta}, {spaces: 2});

  console.log(' - Finished.');
}
