import * as ChildProcess from 'child_process';

export function exec(command: string): Promise<string> {
  return new Promise((resolve, reject) => {
    let childProcess = ChildProcess.exec(command, (error, stdout) => {
      if (error) {
        reject(error);
        return;
      }

      resolve(stdout);
    });

    childProcess.stdout?.pipe(process.stdout);
    childProcess.stderr?.pipe(process.stdout);
  });
}

export async function download(url: string, outPath: string): Promise<void> {
  await exec(`wget -O "${outPath}" ${url}`);
}
