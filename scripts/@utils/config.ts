import * as t from 'io-ts';
import chalk from 'chalk';
import {PathReporter} from 'io-ts/lib/PathReporter';

const configStr = process.env.ENCODE_CONFIG;

if (!configStr) {
  console.error(chalk.red('Error: Config cannot be empty'));
  process.exit(1);
}

const configJSON = JSON.parse(configStr);

const Config = t.type({
  videoURL: t.string,
  subURL: t.string,
  AD_CUT: t.union([t.string, t.undefined]),
  AD_START: t.union([t.string, t.undefined]),
  AD_END: t.union([t.string, t.undefined]),
  output: t.union([t.string, t.undefined]),
});

const result = Config.decode(configJSON);

if (result._tag === 'Left') {
  let messages = PathReporter.report(result);
  console.error(chalk.red('Error: Invalid encode config'));
  console.error(messages.map(message => ` - ${message}`).join('\n'));
  process.exit(1);
}

export const config = result.right;

console.log('Config:', config);

export type Config = typeof config;
