import * as Path from 'path';
import Chalk from 'chalk';
import FS from 'fs-extra';

export const PROJECT_DIR = Path.join(__dirname, '../..');

export const TEMP_DIR = Path.join(PROJECT_DIR, 'temp');
export const OUT_DIR = Path.join(PROJECT_DIR, 'out');

export async function prepareFolders(): Promise<void> {
  console.log(Chalk.green('Preparing folders...'));
  await FS.ensureDir(TEMP_DIR);
  await FS.ensureDir(OUT_DIR);
  console.log(' - Finished.');
}
