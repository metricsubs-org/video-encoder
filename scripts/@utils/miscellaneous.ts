import {URL} from 'url';

export function getBasenameFromURL(url: string | URL): string | undefined {
  if (typeof url === 'string') {
    url = new URL(url);
  }

  let path = url.pathname;
  let slashI = path.lastIndexOf('/');

  if (slashI < 0) {
    slashI = path.lastIndexOf('\\');
  }

  if (slashI < 0) {
    return path;
  }

  let basename = path.slice(slashI + 1);

  if (!basename) {
    return undefined;
  }

  return basename;
}

export function getExtensionFromURL(url: string): string | undefined {
  let dotI = url.lastIndexOf('.');

  if (dotI < 0) {
    return undefined;
  }
  let affix = url.slice(dotI + 1);

  if (!affix) {
    return undefined;
  }

  if (affix.length > 4) {
    return undefined;
  }

  return affix;
}

export function secureFilename(filename: string): string {
  return filename.replace(/[^a-z0-9_\-\.]/gi, '_').toLocaleLowerCase();
}
